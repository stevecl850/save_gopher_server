#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    package buckd_log;
#------------------------------------------------------
    use vars qw( $outFile );

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
      my ($par)=@_;


      return $par;
    }
#------------------------------------------------------
# init
#------------------------------------------------------
    sub init {

      open(buckd_log::outFile, ">>${settings::LOG}") or die "cant open log file";
      my $oldfh = select(buckd_log::outFile); $| = 1; select($oldfh);

      return;
    }
#------------------------------------------------------
# logStr
#------------------------------------------------------
    sub logStr {
      my ($num,$flen,$crap)=@_;

      my $dinfo=$request::gplus.$request::dinfo;
      $dinfo="<$dinfo>" if $dinfo;
      my $str=sprintf("%s %15s:%i %s%s %i - %i %s\n", scalar localtime, $buckd::REMOTE_ADDR , $buckd::REMOTE_PORT, $request::request, $dinfo ,$flen, $num, $crap);
      print buckd_log::outFile $str; 
      print STDOUT $str; 

      return;
    }
#------------------------------------------------------
1;
