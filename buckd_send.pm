#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    package buckd_send;
#------------------------------------------------------
    use vars qw( $error);

    use vars qw( $maxBinSize );
    $maxBinSize=1024*1024*15;
#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
         my ($par)=@_;


         return $par;
    }
#------------------------------------------------------
# lineRange
#------------------------------------------------------
    sub lineRange {

         my $lineFrom=$request::g6vars{lineFrom};
         if ($lineFrom>$buckd_sgbin_send::linesN)
         {
             $lineFrom=$buckd_sgbin_send::linesN;
         }

         my $lineTo;
         if (exists($request::g6vars{lineNum}))
         {
            $lineTo=$lineFrom+$request::g6vars{lineNum};
            if ($lineTo>$buckd_sgbin_send::linesN)
            {
                $lineTo=$buckd_sgbin_send::linesN;
            }
         }
         else
         {
             $lineTo=$buckd_sgbin_send::linesN;
         }
         
         my $shiftStrtLine=buckd_sgbin_send::lineShift($lineFrom);
         my $shiftLastLine=buckd_sgbin_send::lineShift($lineTo);

         return ($shiftStrtLine,$shiftLastLine-$shiftStrtLine);
    }
#------------------------------------------------------
# sendCatalog
#------------------------------------------------------
    sub sendCatalog {
        my ($locator)=@_;

        my $sgbin=buckd_sgbin_update::updateCatalog($locator);
        return -1 if $buckd_sgbin_update::error;
        buckd_sgbin_send::openFile($sgbin);

        my ($shiftStrtLine,$toSend)=lineRange();
        
        if (buckd_send_g6::sendReturnCode($toSend,"1",$buckd_sgbin_send::dataCRC32,$buckd_sgbin_send::linesN,$buckd_sgbin_send::dataSize))
        {
             buckd_sgbin_send::sendLines($buckd_sgbin_send::file,$buckd_sgbin_send::headerBytes+$shiftStrtLine,$toSend);
        }

        buckd_sgbin_send::closeFile();
    
        return $toSend;
    }
#------------------------------------------------------
# sendDirectory
#------------------------------------------------------
    sub sendDirectory {
        my ($locator)=@_;

        my $sgbin=buckd_sgbin_update::updateDirectory($locator);
        return -1 if $buckd_sgbin_update::error;
        buckd_sgbin_send::openFile($sgbin);

        my ($shiftStrtLine,$toSend)=lineRange();

        if (buckd_send_g6::sendReturnCode($toSend,"1",$buckd_sgbin_send::dataCRC32,$buckd_sgbin_send::linesN,$buckd_sgbin_send::dataSize))
        {
#             buckd_sgbin_send::sendLines($buckd_sgbin_send::file,$buckd_sgbin_send::headerBytes,$shiftLastLine);
             buckd_sgbin_send::sendLines($buckd_sgbin_send::file,$buckd_sgbin_send::headerBytes+$shiftStrtLine,$toSend);
        }

        buckd_sgbin_send::closeFile();

        return $toSend;
    }
#------------------------------------------------------
# sendText
#------------------------------------------------------
    sub sendText {
        my ($locator)=@_;

         my $sgbin=buckd_sgbin_update::updateText($locator);
         return -1 if $buckd_sgbin_update::error;
         buckd_sgbin_send::openFile($sgbin,1);

         my ($shiftStrtLine,$toSend)=lineRange();

         buckd_sgbin_send::closeFile();

         if (buckd_send_g6::sendReturnCode($toSend,"0",$buckd_sgbin_send::dataCRC32,$buckd_sgbin_send::linesN,$buckd_sgbin_send::dataSize))
         {
              my $res=open (my $file,"<$locator");
              return -1 unless $res;
              binmode $file;

              buckd_sgbin_send::sendLines($file,$shiftStrtLine,$toSend);

              close($file);
         }
 
        return $toSend;
    }
#------------------------------------------------------
# sendBin
#------------------------------------------------------
    sub sendBin {
        my ($locator)=@_;

        my $dataSize= -s $locator;
        my $dataCRC32;
        if ($dataSize < $maxBinSize)
        {
             my $sgbin=buckd_sgbin_update::updateBin($locator);
             return -1 if $buckd_sgbin_update::error;
             buckd_sgbin_send::openFile($sgbin,1);

             die unless $dataSize==$buckd_sgbin_send::dataSize;

             $dataCRC32=$buckd_sgbin_send::dataCRC32;

             buckd_sgbin_send::closeFile();
        }
        else
        {
             $dataCRC32="non";
        }

        if (buckd_send_g6::sendReturnCode($dataSize,"9",$dataCRC32,0,$dataSize))
        {
             my $res=open (my $file,"<$locator");
             return -1 unless $res;

             binmode $file;

             buckd_sgbin_send::sendLines($file,0,$dataSize);

             close($file);
        }
 
        return $dataSize;
    }
#------------------------------------------------------
# getCRC32
#------------------------------------------------------
    sub getCRC32 {
         my ($filename)=@_;

         if (-d $filename)
         {
              my $fileMap=buckd_sgbin_update::findGophermap($filename);

              return "non" unless buckd::itypebyext_crc($fileMap);

              my $sgbin=buckd_sgbin_update::updateDirectory($filename);
              return "non" if $buckd_sgbin_update::error;

              buckd_sgbin_send::openFile($sgbin);
              buckd_sgbin_send::closeFile();

              return $buckd_sgbin_send::dataCRC32;
         }
         elsif (-f $filename)
         {

               my $typeExt=buckd::itypebyext_send($filename);
               if ( $typeExt eq "1")
               {
                    return "non" unless buckd::itypebyext_crc($filename);

                    my $sgbin=buckd_sgbin_update::updateCatalog($filename);
                    return "non" if $buckd_sgbin_update::error;
                    buckd_sgbin_send::openFile($sgbin);
                    buckd_sgbin_send::closeFile();
                    return $buckd_sgbin_send::dataCRC32;
               }
               elsif ( $typeExt eq "0")
               {
                    my $sgbin=buckd_sgbin_update::updateText($filename);
                    return "non" if $buckd_sgbin_update::error;
                    buckd_sgbin_send::openFile($sgbin);
                    buckd_sgbin_send::closeFile();
                    return $buckd_sgbin_send::dataCRC32;
               }
               elsif ( $typeExt eq "sgbin")
               {
                    return "non";
               }
               else
               {
                    if (-s $filename > $maxBinSize)
                    {
                        return "non";
                    }
                    my $sgbin=buckd_sgbin_update::updateBin($filename);
                    return "non" if $buckd_sgbin_update::error;
                    buckd_sgbin_send::openFile($sgbin);
                    buckd_sgbin_send::closeFile();
                    return $buckd_sgbin_send::dataCRC32;
               }
         }


         return "non";
    }
#------------------------------------------------------
1;
