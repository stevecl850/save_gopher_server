#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

    use crc32;

#------------------------------------------------------
    package request;

    use vars qw( $request $dinfo $gplus $gopher6 $g6data $g6crc32 $g6form);

    use vars qw( @g6CRCs @g6flags $g6command @g6URLs );

    use vars qw( %g6vars );

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
      my ($par)=@_;


      return $par;
    }
#------------------------------------------------------
# parseRequest
#------------------------------------------------------
    sub parseRequest {
      my ($orq)=@_;

      $orq =~ s/\r\n?$//;

      ($request, $dinfo, $gopher6) = split(/[\?\t]/, $orq, 3);


      $gplus="";
      if ($dinfo =~ m{^[\$\+\!]})
      {
         $gplus="+";
         $dinfo=substr($dinfo,1);
         return;
      }

      $request =~ s/%([a-fA-F0-9]{2})/pack("H2", $1)/eg;
      # $request must be absolute ...
      $request = "/$request" if ($request !~ m#^/#);
      $request = canon($request);

      # this is a kludge on %-escaping. try to see if needed first.
      # otherwise arguments like +, % that aren't encoded from gopher
      # clients will be eaten!
      if ($dinfo =~ /%([a-fA-F0-9]{2})/) {
            $dinfo =~ s/\+/ /g;
            $dinfo =~ s/%([a-fA-F0-9]{2})/pack("H2", $1)/eg;
      }

      return;
    }
#------------------------------------------------------
# readG6Data
#------------------------------------------------------
    sub readG6Data {
      my ($num)=@_;

      my ($datIn,$n);
      $n=read(DATAS,$datIn,$num);

#      $g6data .= $datIn;
      crc32::addStr($datIn);

      return "" if $n != $num;

      return $datIn;
    }
#------------------------------------------------------
# readGopher6
#------------------------------------------------------
    sub readGopher6 {
      local (*DATAS)=@_;

      $g6data="";
      $g6command="";
      $g6form="";
      @g6CRCs=();

      @g6flags=(); 

      @g6URLs=(); 

      %g6vars=();

      crc32::init();
      crc32::addStr($gopher6);

      # flags:
      # 1  - send CRC if CRC found
      # 2  - send CRC if CRC not found
      # 0  - does nothing
      # 3  - send DATA if CRC found
      # 4  - send DATA if CRC not found
      #    - send DATA if CRC unknown
      # 5  - send overall number of lines in file (or varB=3 of unknown)
      # 6  - send overall number of bytes in file (or varB=3 of unknown)
      # commands:  (a .. p)
      # a  - get request
      #   default is send nothing never
      # z  - CRC
      # y  - list of CRC (up to 255)
      # x  - locator (up to 255 chars)
      # w  - line from 32bit
      # v  - line from 16bit
      # u  - line from 64bit
      # t  - lines     16bit
      # s  - lines     8 bit
      # r  - FORM data in zero terminated format
      # makro codes:
      # A=a4z     data request with one CRC given
      # B=a12     CRC request
      # C=a14y    data request with list of CRC given
      # D=a34     data request
      # retutn code:
      # varA: 0-CRC not found; 1-CRC found; 2 - CRC unknown (same like not found, but CRC never sends)
      # varB: 0-length <65536 (16 bit); 1-length >65536 (32 bit); 2-length >65536^2 (64 bit); 3-chanked encoding with chank length <65536 (16 bit)
      # returnCode=varB*4+varA
      # if returnCode >=16 - it is a error code
      my $gopher6temp=$gopher6;
      while($gopher6temp)
      {
          my $g6=substr($gopher6temp,0,1);

          $gopher6temp=substr($gopher6temp,1);

          if ($g6 ge "0" and $g6 le "9")           # flags
          {
              $g6flags[$g6]=1; 
          }
          elsif ($g6 ge "a" and $g6 le "p")           # commands
          {
              $g6command=$g6; 
          }
          elsif ($g6 eq "A")       # A=4z     data request with one CRC given
          {
               $gopher6temp="a4z".$gopher6temp;
          }
          elsif ($g6 eq "B")       # B=12    CRC request
          {
               $gopher6temp="a12".$gopher6temp;
          }
          elsif ($g6 eq "C")       # C=14y    data request with list of CRC given
          {
               $gopher6temp="a14y".$gopher6temp;
          }
          elsif ($g6 eq "D")       # D=34    data request
          {
               $gopher6temp="a34".$gopher6temp;
          }
          elsif ($g6 eq "z") # g  - CRC
          {
              my $datIn=readG6Data(4);
              return 1 unless $datIn;

              push(@g6CRCs,unpack("L",$datIn));

          }
          elsif ($g6 eq "y") # h  - list of CRC (up to 255)
          {
              my $datIn=readG6Data(1);return 1 unless $datIn;
              my $num=unpack("C",$datIn);

              for(my $i=0;$i<$num;$i++)
              {
                   $datIn=readG6Data(4);return 1 unless $datIn;

                   push(@g6CRCs,unpack("L",$datIn));
              }

          }
          elsif ($g6 eq "x") # k  - locator (up to 255 chars)
          {
              my $datIn=readG6Data(1);return 1 unless $datIn;
              my $num=unpack("C",$datIn);

              $datIn=readG6Data($num);return 1 unless $datIn;

              push(@g6URLs,$datIn);

          }
          elsif ($g6 eq "w")  # w  - line from   32bit
          {
              my $datIn=readG6Data(4);
              return 1 unless $datIn;

              $g6vars{lineFrom}=unpack("L",$datIn);

          }
          elsif ($g6 eq "v")  # v  - lines  16bit
          {
              my $datIn=readG6Data(2);
              return 1 unless $datIn;

              $g6vars{lineNum}=unpack("S",$datIn);

          }
          elsif ($g6 eq "r")  # r  - FORM data in zero terminated format
          {

              for(;;)
              {
                  my $datIn=readG6Data(1);
                  return 1 if $datIn eq "";
                  last if $datIn eq chr(0);
                  $g6form.=$datIn;
              }

          }
          else
          {
              print STDOUT "G6error:$g6\n";
              return 1;
          }
      }
#      crc32::addStr($g6data);
      $g6crc32=crc32::result();
#      $g6crc32=crc32::strcrc32($gopher6.$g6data);

      print STDOUT "g6command:$g6command\n";
      print STDOUT "g6flags:",::dump(\@g6flags),"\n";
      print STDOUT "g6vars:",::dump(\%g6vars),"\n";
      print STDOUT "g6form:$g6form]\n";

      return 0;
    }
#------------------------------------------------------
# readRequest
#------------------------------------------------------
    sub readRequest {
      local (*DATAS)=@_;

      my $grequest = <DATAS> ;

      print STDOUT ::dump($grequest),"\n";

      request::parseRequest($grequest);

      my $res=readGopher6(*DATAS);

      if ($gopher6)
      {
#          print STDOUT "G6DAT:",unpack( 'H*', $g6data ),"\n";
          print STDOUT "CRC32:",unpack( 'H*', pack("L",$g6crc32 )),"\n";
      }

      $gopher6="" if $res != 0;
      $gopher6="" if $g6command ne "a";

      if ($gopher6 and $g6form)
      {
         my $FORMdirectory=$settings::DIR_FORM."/".sprintf("%08X",crc32::strcrc32($request));
         mkdir($FORMdirectory);
         my $FORMname=timeStr();
         my $FORMnum=0;
         my $fileName;
         for(;;)
         {
             $fileName=$FORMdirectory."/".$FORMname."_".sprintf("%06i",$FORMnum).".G6E";
             last unless -f $fileName;
             $FORMnum++;
         }
         strToFile($fileName,$g6form);

      }
      return;
    }
#------------------------------------------------------
#  strToFile
#------------------------------------------------------
    sub strToFile {
        my ($filename,$str)=@_;

        open (FileD,">$filename") or die "cant open '$filename'";
        binmode FileD;
        print FileD $str;
        close FileD;

        return;
    }
#------------------------------------------------------
# timeStr
#------------------------------------------------------
    sub timeStr {

        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime();

        $year+=1900;
        $mon++;

        ($mon,$mday,$hour,$min,$sec)=map(sprintf("%02i",$_),($mon,$mday,$hour,$min,$sec));

        return "$year-$mon-${mday}_$hour-$min-$sec";

    }
#------------------------------------------------------
# canon
#------------------------------------------------------
sub canon {
      my ($str) = (@_);
      1 while $str =~ s#//#/#;
      1 while $str =~ s#/\.(/|$)#$1#;
      1 while $str =~ s#/[^/]*/\.\.(/|$)#$1#;
      1 while $str =~ s#^/\.\.(/|$)#$1#;
      return $str;
}
#------------------------------------------------------
1;
