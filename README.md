# README #

Safe, secure and standalone portable gopher server

no cgi support

only serves files in given data directory

G6 extention:

1. requests for line ranges
2. CRC32 in requests to prevent sending files in cache
3. transfer size and error codes
4. user filling forms
5. realtime data streaming
