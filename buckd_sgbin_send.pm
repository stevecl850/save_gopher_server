#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;

#    use crc32;

    package buckd_sgbin_send;
#------------------------------------------------------
    use vars qw( $error);

    use vars qw( $headerBytes);

    $headerBytes=4+4+4;

    use vars qw( $file $linesShift );

    use vars qw( $dataCRC32 $linesN $dataSize);

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
         my ($par)=@_;


         return $par;
    }
#------------------------------------------------------
# openFile
#------------------------------------------------------
    sub openFile {
         my ($filename,$isText)=@_;

         open ($file,"<$filename") or die "cant open '$filename'";
         binmode $file;

         my $header="";
         read($file, $header, $headerBytes);
         ($dataCRC32, $linesN, $dataSize)=unpack("LLL",$header);

         $linesShift=$headerBytes;

         $linesShift+=$dataSize unless $isText;


         return;
    }
#------------------------------------------------------
# closeFile
#------------------------------------------------------
    sub closeFile {

         close $file;

         return;
    }
#------------------------------------------------------
# lineShift
#------------------------------------------------------
    sub lineShift {
         my ($lineN)=@_;

         die if $lineN>$linesN or $lineN<0;

         seek($file,$linesShift+$lineN*4,0);
         my $data;
         read($file, $data, 4);

         return unpack("L",$data);
    }

#------------------------------------------------------
# min
#------------------------------------------------------
    sub min {
         my ($a,$b)=@_;

         return  $a > $b ? $b : $a;
    }
#------------------------------------------------------
# sendLines
#------------------------------------------------------
    sub sendLines {
         my ($f, $shiftStart, $toSend)=@_;

         die if $toSend<0;

         seek($f,$shiftStart,0);

         while($toSend) 
         {
               my $data;
               read($f, $data, min(16384,$toSend) );

               $toSend -= length($data);
               print $data;
         }

         return;
    }
#------------------------------------------------------
1;
